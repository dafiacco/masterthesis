
from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import glob
import os
#import TauAnalysis.utils.utils_Zmumu.functions as fn 

#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/THOR_ntuples/'

#files = os.scandir(path = path)
file_counter = 0

###Definition of histograms
xlow = 0
xup = 5
nbins = 5
h_NumTrackLeadPerEvent = ROOT.TH1F("NumTrackLeadPerEvent", "NumTrackLeadPerEvent", nbins, xlow, xup)
h_NumTrackSubleadPerEvent = ROOT.TH1F("NumSublrackLeadPerEvent", "NumTrackSubleadPerEvent", nbins, xlow, xup)
xlow = 0
xup = 60000
nbins = 90
h_LeadTauTracksPt = ROOT.TH1F("LeadTauTracksPt", "LeadTauTracksPt", nbins, xlow, xup) # set log y
xup = 50000
h_SubleadTauTracksPt = ROOT.TH1F("SubleadTauTracksPt", "SubleadTauTracksPt", nbins, xlow, xup) # set log y
xlow = 0
xup = 100000
h_LeadTauTracksE = ROOT.TH1F("LeadTauTracksE", "LeadTauTracksE", nbins, xlow, xup) # set log y
xup = 200000
h_SubleadTauTracksE = ROOT.TH1F("SubleadTauTracksE", "SubleadTauTracksE", nbins, xlow, xup) # set log y
xlow = -4
xup = 4
nbins = 70
h_LeadTauTracksPhi = ROOT.TH1F("LeadTauTracksPhi", "LeadTauTracksPhi", nbins, xlow, xup)
h_SubleadTauTracksPhi = ROOT.TH1F("SubleadTauTracksPhi", "SubleadTauTracksPhi", nbins, xlow, xup)
xlow = -3
xup = 3
h_LeadTauTracksEta = ROOT.TH1F("LeadTauTracksEta", "LeadTauTracksEta", nbins, xlow, xup)
h_SubleadTauTracksEta = ROOT.TH1F("SubleadTauTracksEta", "SubleadTauTracksEta", nbins, xlow, xup)
xlow = -3
xup = 3
#h_LeadTauAllTracksEta = ROOT.TH1F("LeadTauAllTracksEta", "LeadTauAllTracksEta", nbins, xlow, xup)
xlow = -4
xup = 4
#h_LeadTauAllTracksPhi = ROOT.TH1F("LeadTauAllTracksPhi", "LeadTauAllTracksPhi", nbins, xlow, xup)
xlow = 0
xup = 5000000
#h_LeadTauAllTracksE = ROOT.TH1F("LeadTauAllTracksE", "LeadTauAllTracksE", nbins, xlow, xup)# set log y
xlow = 0
xup = 5000000
#h_LeadTauAllTracksPt = ROOT.TH1F("LeadTauAllTracksPt", "LeadTauAllTracksPt", nbins, xlow, xup)# set log y
xlow = -3
xup = 3
#h_SubleadTauAllTracksEta = ROOT.TH1F("SubleadTauAllTracksEta", "SubleadTauAllTracksEta", nbins, xlow, xup)
xlow = -4
xup = 4
#h_SubleadTauAllTracksPhi = ROOT.TH1F("SubleadTauAllTracksPhi", "SubleadTauAllTracksPhi", nbins, xlow, xup)
xlow = 0
xup = 5000000
#h_SubleadTauAllTracksE = ROOT.TH1F("SubleadTauAllTracksE", "SubleadTauAllTracksE", nbins, xlow, xup)# set log y
xlow = 0
xup = 5000000
#h_SubleadTauAllTracksPt = ROOT.TH1F("SubleadTauAllTracksPt", "SubleadTauAllTracksPt", nbins, xlow, xup)# set log y


print("In the directory there are ",len(glob.glob(path+"*.root"))," root files")
Entries = 0
FakeEntry = 0
### Loop over the ntuples in the directory
for f in glob.glob(path+"*.root"): #files: 2022-01-20_histograms.root
    if f != 'naan':             #.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(f) #path+f.name)
        file_counter += 1
        print("processing file: ", file_counter, "...")
        Entries += input_file.T_Zp2thh_NOMINAL.GetEntries()
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            #weight = evt.mcWeight #aggiungere altre variabili
            #TauCharge = evt.TauCharge
            TauEta = evt.TauEta
            TauPhi = evt.TauPhi
            TauNAllTracks = evt.TauNAllTracks
            TauNTracks = evt.TauNTracks
            #if len(TauCharge) > 1 :
            #    if TauCharge[0] == TauCharge[1] :
            #        FakeEntry += 1
            LeadTauTracksPt = evt.LeadTauTracksPt
            LeadTauTracksPhi = evt.LeadTauTracksPhi
            LeadTauTracksEta = evt.LeadTauTracksEta
            SubleadTauTracksEta = evt.SubleadTauTracksEta
            SubleadTauTracksPhi = evt.SubleadTauTracksPhi
            SubleadTauTracksPt = evt.SubleadTauTracksPt
            h_NumTrackLeadPerEvent.Fill( len(LeadTauTracksPt) )
            for i in range( len(LeadTauTracksPt) ):
                h_LeadTauTracksPt.Fill(LeadTauTracksPt[i]/1000,weight)
                h_LeadTauTracksPhi.Fill(LeadTauTracksPhi[i],weight)
                h_LeadTauTracksEta.Fill(LeadTauTracksEta[i],weight)
            for i in range( len(SubleadTauTracksPt) ):
                h_SubleadTauTracksEta.Fill(SubleadTauTracksEta[i],weight)
                h_SubleadTauTracksPhi.Fill(SubleadTauTracksPhi[i],weight)
                h_SubleadTauTracksPt.Fill(SubleadTauTracksPt[i]/1000,weight)
            h_NumTrackSubleadPerEvent.Fill( len(SubleadTauTracksPt) )
        input_file.Close()
#files.close()
print("In the directory there are ", Entries," Total Entries.")
print("In the directory there are ", FakeEntry ," Fake Entries in total.")

canv = ROOT.TCanvas("canvas","canvas1")
#ROOT.gStyle.SetOptStat(0) #toglie la tabella
h_NumTrackLeadPerEvent.GetXaxis().SetTitle("Number of Tracks in Leading Tau")
h_NumTrackLeadPerEvent.GetYaxis().SetTitle("Events")
h_NumTrackLeadPerEvent.Draw("hist")
canv.SaveAs("histograms_MC20/NumTrackLeadPerEvent.png")

h_NumTrackSubleadPerEvent.GetXaxis().SetTitle("Number of Tracks in Sub-leading Tau")
h_NumTrackSubleadPerEvent.GetYaxis().SetTitle("Events")
h_NumTrackSubleadPerEvent.Draw("hist")
canv.SaveAs("histograms_MC20/NumTrackSubleadPerEvent.png")


h_LeadTauTracksPhi.GetXaxis().SetTitle("Phi of tracks of Leading Tau")
h_LeadTauTracksPhi.GetYaxis().SetTitle("Events")
h_LeadTauTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauTracksPhi.png")

h_SubleadTauTracksPhi.GetXaxis().SetTitle("Phi of tracks of Sub-leading Tau")
h_SubleadTauTracksPhi.GetYaxis().SetTitle("Events")
h_SubleadTauTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauTracksPhi.png")

h_LeadTauTracksEta.GetXaxis().SetTitle("Eta of tracks of Leading Tau")
h_LeadTauTracksEta.GetYaxis().SetTitle("Events")
h_LeadTauTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauTracksEta.png")

h_SubleadTauTracksEta.GetXaxis().SetTitle("Eta of tracks of Sub-leading Tau")
h_SubleadTauTracksEta.GetYaxis().SetTitle("Events")
h_SubleadTauTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauTracksEta.png")
"""
h_LeadTauAllTracksEta.GetXaxis().SetTitle("Eta of All Tracks of Leading Tau")
h_LeadTauAllTracksEta.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksEta.png")

h_LeadTauAllTracksPhi.GetXaxis().SetTitle("Phi of All Tracks of Leading Tau")
h_LeadTauAllTracksPhi.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksPhi.png")

h_SubleadTauAllTracksEta.GetXaxis().SetTitle("Eta of All Tracks of Sub-leading Tau")
h_SubleadTauAllTracksEta.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksEta.png")

h_SubleadTauAllTracksPhi.GetXaxis().SetTitle("Phi of All Tracks of Sub-leading Tau")
h_SubleadTauAllTracksPhi.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksPhi.png")
"""
canv.SetLogy()

h_LeadTauTracksPt.GetXaxis().SetTitle("Pt of core tracks of Leading Tau [GeV]")
h_LeadTauTracksPt.GetYaxis().SetTitle("Events")
h_LeadTauTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauTracksPt.png")

h_SubleadTauTracksPt.GetXaxis().SetTitle("Pt of core tracks of Sub-leading Tau [GeV]")
h_SubleadTauTracksPt.GetYaxis().SetTitle("Events")
h_SubleadTauTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauTracksPt.png")

h_LeadTauTracksE.GetXaxis().SetTitle("Energy of core tracks of Leading Tau [GeV]")
h_LeadTauTracksE.GetYaxis().SetTitle("Events")
h_LeadTauTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauTracksE.png")

h_SubleadTauTracksE.GetXaxis().SetTitle("Energy of core tracks Sub-leading Tau [GeV]")
h_SubleadTauTracksE.GetYaxis().SetTitle("Events")
h_SubleadTauTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauTracksE.png")
"""
h_LeadTauAllTracksPt.GetXaxis().SetTitle("Pt of All Tracks Leading Tau [GeV]")
h_LeadTauAllTracksPt.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksPt.png")

h_LeadTauAllTracksE.GetXaxis().SetTitle("Energy of All Tracks of Leading Tau [GeV]")
h_LeadTauAllTracksE.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksE.png")

h_SubleadTauAllTracksPt.GetXaxis().SetTitle("Pt of All Tracks of Sub-leading Tau [GeV]")
h_SubleadTauAllTracksPt.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksPt.png")

h_SubleadTauAllTracksE.GetXaxis().SetTitle("Energy of All Tracks of Sub-leading Tau [GeV]")
h_SubleadTauAllTracksE.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksE.png")
"""
