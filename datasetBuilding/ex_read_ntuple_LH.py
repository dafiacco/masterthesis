from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import os
#import TauAnalysis.utils.utils_Zmumu.functions as fn


path = '/eos/user/f/fmorodei/public/DAOD_PHYS/ntuples/Ztautau/'
files = os.scandir(path = path)

file_counter = 0

xlow=-2
xup=2
n_bins = 80
h_muh_cosphimc = ROOT.TH1F("muh_cosphimc", "cosphimc", n_bins, xlow, xup)
h_eh_cosphimc = ROOT.TH1F("eh_cosphimc", "cosphimc", n_bins, xlow, xup)

xlow = 0
xup = 150
n_bins = 30
h_muh_mtmc = ROOT.TH1F("muh_mtmc", "mtmc", n_bins, xlow, xup)
h_eh_mtmc = ROOT.TH1F("eh_mtmc", "mtmc", n_bins, xlow, xup)


### Loop over the ntuples in the directory
for f in files:
    if f.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(path+f.name)
        #input_file=ROOT.TFile.Open(path)
        file_counter += 1
        if file_counter > 9:
            print("processing file: ", file_counter, "...")
            for evt in input_file.T_Zp2tlh_NOMINAL:  ## loop over the events
                weight = evt.mcWeight #aggiungere altre variabili
                muonPt = evt.MuonPt
                elePt = evt.ElePt
                if len(muonPt)>0 :
                    tauPhi = evt.JetPhi
                    muonPhi = evt.MuonPhi
                    metPhi = evt.MET_phi
                    print("len muphi and jetphi")
                    print(len(muonPhi))
                    print(len(tauPhi))
                    tmp = np.cos(muonPhi[0] - metPhi)
                    tmp2 = np.cos(tauPhi[0] - metPhi)
                    h_muh_cosphimc.Fill( tmp + tmp2, weight)
                    metEt = evt.MET_met #suppongo sia il modulo
                    h_muh_mtmc.Fill( np.sqrt( 2*muonPt[0] * metEt * (1 - tmp) )/1000, weight)
                elif len(elePt)>0:
                    elePhi = evt.ElePhi
                    tauPhi = evt.JetPhi
                    print("len elephi and jetphi")
                    print(len(elePhi))
                    print(len(tauPhi))
                    metPhi = evt.MET_phi
                    tmp = np.cos(elePhi[0] - metPhi)
                    h_eh_cosphimc.Fill( tmp + np.cos(tauPhi[0] - metPhi), weight)
                    metEt = evt.MET_met #suppongo sia il modulo
                    elePt = evt.ElePt
                    h_eh_mtmc.Fill( np.sqrt( 2*elePt[0] * metEt * (1 - tmp) )/1000 , weight)
        input_file.Close()
files.close()

canv = ROOT.TCanvas("canvas","canvas1")
pad1 = ROOT.TPad("pad1","tau mass", 0.02, 0.51, 0.98, 0.98)
pad2 = ROOT.TPad("pad2","tau eta", 0.02, 0.02, 0.98, 0.49)
pad1.Draw()
pad2.Draw()

#ROOT.gStyle.SetOptStat(0)
pad1.cd()
h_muh_cosphimc.GetXaxis().SetTitle("cos(DeltaPhi)")
h_muh_cosphimc.GetYaxis().SetTitle("Events")
h_muh_cosphimc.Draw("hist")
canv.Update()

pad2.cd()
h_eh_cosphimc.GetXaxis().SetTitle("cos(DeltaPhi)")
h_eh_cosphimc.GetYaxis().SetTitle("Events")
h_eh_cosphimc.Draw("hist")
canv.Update()

canv.SaveAs("histograms/tau_LH_cosDPhi.png")

canv = ROOT.TCanvas("canvas","canvas1")
pad1 = ROOT.TPad("pad1","tau mass", 0.02, 0.51, 0.98, 0.98)
pad2 = ROOT.TPad("pad2","tau eta", 0.02, 0.02, 0.98, 0.49)
pad1.Draw()
pad2.Draw()

#ROOT.gStyle.SetOptStat(0)
pad1.cd()
h_muh_mtmc.GetXaxis().SetTitle("mT [GeV]")
h_muh_mtmc.GetYaxis().SetTitle("Events")
h_muh_mtmc.Draw("hist")
canv.Update()

pad2.cd()
h_eh_mtmc.GetXaxis().SetTitle("mT [GeV]")
h_eh_mtmc.GetYaxis().SetTitle("Events")
h_eh_mtmc.Draw("hist")
canv.Update()

canv.SaveAs("histograms/tau_LH_mT.png")
