from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import os
import glob
#import TauAnalysis.utils.utils_Zmumu.functions as fn


#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/THOR_ntuples/'
path = '/eos/user/f/fmorodei/public/ntuple4Davide/THOR_ntuples/Ztautau/'

file_counter = 0
'''
xlow=-2
xup=2
n_bins = 80
h_muh_cosphimc = ROOT.TH1F("muh_cosphimc", "cosphimc", n_bins, xlow, xup)
h_eh_cosphimc = ROOT.TH1F("eh_cosphimc", "cosphimc", n_bins, xlow, xup)

xlow = 0
xup = 150
n_bins = 30
h_muh_mtmc = ROOT.TH1F("muh_mtmc", "mtmc", n_bins, xlow, xup)
h_eh_mtmc = ROOT.TH1F("eh_mtmc", "mtmc", n_bins, xlow, xup)
'''


xlow = 0
xup = 20
nbins = 20
h_len_LeadTauTracksEta = ROOT.TH1F("NumTrackLeadPerEvent", "NumTrackLeadPerEvent", nbins, xlow, xup)
h_len_SubleadTauTracksEta = ROOT.TH1F("NumSublrackLeadPerEvent", "NumTrackSubleadPerEvent", nbins, xlow, xup)
xlow = 0
xup = 10
nbins = 10
h_LeadTauNTracks = ROOT.TH1F("LeadTauNTracks", "LeadTauNTracks", nbins, xlow, xup)
h_LeadTauNTracksIsolation = ROOT.TH1F("LeadTauNTracksIsolation", "LeadTauNTracksIsolation", nbins, xlow, xup)
h_SubleadTauNTracks = ROOT.TH1F("SubleadTauNTracks", "SubleadTauNTracks", nbins, xlow, xup)
h_SubleadTauNTracksIsolation = ROOT.TH1F("SubleadTauNTracksIsolation", "SubleadTauNTracksIsolation", nbins, xlow, xup)
xup = 35
nbins = 35
h_LeadTauNClusters = ROOT.TH1F("LeadTauNClusters", "LeadTauNClusters", nbins, xlow, xup)
h_SubleadTauNClusters = ROOT.TH1F("SubleadTauNClusters", "SubleadTauNClusters", nbins, xlow, xup)

#output_file=ROOT.TFile.Open('/afs/cern.ch/user/d/dafiacco/eos/analysis/histograms/TTree_output.root')
print("In the directory there are ",len(glob.glob(path+"*.root"))," root files")
### Loop over the ntuples in the directory
Event_counter = 0
Tau_HH_counter = 0
Tau_LH_counter = 0
FakeEntry_HH = 0
FakeEntry_LH = 0
for f in glob.glob(path+"*.root"): #files:
    if f != 'naan':             #.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(f)
        #input_file=ROOT.TFile.Open(path)
        file_counter += 1
        print("processing file: ", file_counter, "...")
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            Event_counter += 1
            #weight = evt.mcWeight #aggiungere altre variabili
            #TauCharge = evt.TauCharge
            #Tau_HH_counter += len(TauCharge)
            #Tau_LH_counter += len(TauCharge)
            TauIsTruthMatched = evt.TauIsTruthMatched
            LeadTauTracks_nIBPHits = evt.LeadTauTracks_nInnermostPixelHits
            LeadTauTracks_nPixelHits = evt.LeadTauTracks_nPixelHits
            LeadTauTracks_nSCTHits = evt.LeadTauTracks_nSCTHits
            print(LeadTauTracks_nSCTHits)
            print(type(LeadTauTracks_nSCTHits))
            print(LeadTauTracks_nSCTHits[0])
            print(ord(LeadTauTracks_nSCTHits[0]))
            print(ord(LeadTauTracks_nSCTHits[1]))
            print(type(LeadTauTracks_nSCTHits[0]))
            TauPhi = evt.TauPhi
            #print(type(TauPhi))
            #print(TauPhi)
            #print(TauPhi[0])
            #print(type(TauPhi[0]))
            LeadTauTracksEta = evt.LeadTauTracksEta
            #LeadTauAllTracksPt = evt.LeadTauAllTracksPt ###Modified for test
            SubleadTauTracksEta = evt.SubleadTauTracksEta
            TauNTracks = evt.TauNTracks
            TauNTracksIsolation = evt.TauNTracksIsolation
            TauNClusters = evt.TauNClusters
            LeadTauCluster_dEta = evt.LeadTauCluster_dEta
            h_len_LeadTauTracksEta.Fill( len(LeadTauTracksEta) )
            h_len_SubleadTauTracksEta.Fill( len(SubleadTauTracksEta) )
            h_LeadTauNTracksIsolation.Fill( TauNTracksIsolation[0] )
            h_LeadTauNTracks.Fill( TauNTracks[0] )
            h_LeadTauNClusters.Fill( TauNClusters[0] )
            h_SubleadTauNTracksIsolation.Fill( TauNTracksIsolation[1] )
            h_SubleadTauNTracks.Fill( TauNTracks[1] )
            h_SubleadTauNClusters.Fill( TauNClusters[1] )
            #print("TauNClusters = ",TauNClusters )
            #print("len LeadTauCluster_dEta = ",len( LeadTauCluster_dEta ) )
            #print("LeadTauCluster_dEta = ",LeadTauCluster_dEta )
            TauSeedJetPt = evt.TauSeedJetPt
            #print("len TauSeedJetPt = ", len(TauSeedJetPt) )
            #print("len TauPhi = ", len(TauPhi) )
            #print("TauSeedJetPt = ",TauSeedJetPt )
        input_file.Close()
print("In the directory there are ", Event_counter," Total Entries.")
#print("In the directory there are ", Tau_LH_counter ," Tau in LH in total.")
#print("In the directory there are ", FakeEntry_HH ," HH Fake Entries in total.")
#print("In the directory there are ", FakeEntry_LH ," LH Fake Entries in total.")

canv = ROOT.TCanvas("canvas","canvas1")
#ROOT.gStyle.SetOptStat(0) #toglie la tabella
h_len_LeadTauTracksEta.GetXaxis().SetTitle("Number of Tracks in Leading Tau")
h_len_LeadTauTracksEta.GetYaxis().SetTitle("Events")
h_len_LeadTauTracksEta.Draw("hist")
canv.SaveAs("histograms_mc20/len_LeadTauTracksEta.png")

h_len_SubleadTauTracksEta.GetXaxis().SetTitle("Number of Tracks in Sub-leading Tau")
h_len_SubleadTauTracksEta.GetYaxis().SetTitle("Events")
h_len_SubleadTauTracksEta.Draw("hist")
canv.SaveAs("histograms_mc20/len_SubleadTauTracksEta.png")

h_LeadTauNTracks.GetXaxis().SetTitle("Number of Tracks in Leading Tau")
h_LeadTauNTracks.GetYaxis().SetTitle("Events")
h_LeadTauNTracks.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauNTracks.png")

h_LeadTauNTracksIsolation.GetXaxis().SetTitle("Number of Tracks in leading Tau")
h_LeadTauNTracksIsolation.GetYaxis().SetTitle("Events")
h_LeadTauNTracksIsolation.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauNTracksIsolation.png")

h_LeadTauNClusters.GetXaxis().SetTitle("Number of Clusters in Leading Tau")
h_LeadTauNClusters.GetYaxis().SetTitle("Events")
h_LeadTauNClusters.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauNClusters.png")

h_SubleadTauNTracks.GetXaxis().SetTitle("Number of Tracks in Sub-Leading Tau")
h_SubleadTauNTracks.GetYaxis().SetTitle("Events")
h_SubleadTauNTracks.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauNTracks.png")

h_SubleadTauNTracksIsolation.GetXaxis().SetTitle("Number of Tracks in Sub-leading Tau")
h_SubleadTauNTracksIsolation.GetYaxis().SetTitle("Events")
h_SubleadTauNTracksIsolation.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauNTracksIsolation.png")

h_SubleadTauNClusters.GetXaxis().SetTitle("Number of Clusters in Sub-Leading Tau")
h_SubleadTauNClusters.GetYaxis().SetTitle("Events")
h_SubleadTauNClusters.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauNClusters.png")
