# analysis.py

from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import os
#import TauAnalysis.utils.utils_Zmumu.functions as fn

#path delle ntuple LH degli eventi Ztautau, con TTree di interesse: T_Zp2tlh_NOMINAL
#path = '/eos/user/f/fmorodei/public/DAOD_PHYS/ntuples/Ztautau/'

#path delle ntuple HH degli eventi Ztautau, con TTree di interesse: T_Zp2thh_NOMINAL
path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'

#path delle ntuple del fondo multijet di QCD, con TTree di interesse: T_Zp2thh_NOMINAL and T_Zp2tlh_NOMINAL
path = '/afs/cern.ch/user/f/fmorodei/eos/public/DAOD_PHYS/ntuples/multijet'

files = os.scandir(path = path)

file_counter = 0


# codice copiando da quello di Federico per aprire il TTree
