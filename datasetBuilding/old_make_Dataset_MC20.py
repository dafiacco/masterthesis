import ROOT
import glob
import numpy as np


#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/multijet/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'
path = '/eos/user/f/fmorodei/public/ntuple4Davide/THOR_ntuples/QCD/' #Ztautau

file_counter = 0
tau_counter = 0
Fake_tau_counter = 0
CHARGE_SELECTOR = 1
LEPTON_SELECTOR = 0
array = []
tracks = []

print("In the directory there are ",len(glob.glob(path+"*histograms.root"))," root files")
for f in glob.glob(path+"*histograms.root"): #files:
    input_file=ROOT.TFile.Open(f) #path+f.name)
    file_counter += 1
    print("processing file: ", file_counter, "...")
    for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
        TauEta = evt.TauEta
        TauPt = evt.TauPt
        TauNTracks = evt.TauNTracks
        if CHARGE_SELECTOR == 1 or CHARGE_SELECTOR == 0:#It works on QCD events and Tau events, because it does same operations
            if len(TauEta) == 2 :
                if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                    EVENT_SELECTOR = 1
                    if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                        EVENT_SELECTOR = 2
                else :
                    EVENT_SELECTOR = 0 #It appens when one tau-filter is False
            else :
                EVENT_SELECTOR = 0 #It appens if we do't have two true tau
            if EVENT_SELECTOR == 2 and LEPTON_SELECTOR == 0: #It works on both fake-tau
                Fake_tau_counter += 2
                tau_counter += 2
                TauPt = evt.TauPt
                TauE = evt.TauE
                TauNTracks = evt.TauNTracks
                TauNClusters = evt.TauNClusters
                TauSeedJetPt = evt.TauSeedJetPt
                etOverPtLeadTrk = evt.etOverPtLeadTrk
                massTrkSys = evt.massTrkSys
                trFlightPathSig = evt.trFlightPathSig
                centFrac = evt.centFrac
                dRmax = evt.dRmax
                ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                array.append( [TauPt[0], TauE[0], TauNTracks[0], TauNClusters[0], TauSeedJetPt[0], etOverPtLeadTrk[0], massTrkSys[0],
                              trFlightPathSig[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
                array.append( [TauPt[1], TauE[1], TauNTracks[1], TauNClusters[1], TauSeedJetPt[1], etOverPtLeadTrk[1], massTrkSys[1],                              trFlightPathSig[1], centFrac[1], dRmax[1], ClustersMeanCenterLambda[1], ClustersMeanSecondLambda[1] ] )
    input_file.Close()
if CHARGE_SELECTOR == 1:
    array = np.reshape(array, (Fake_tau_counter, 12) )
else:
    array = np.reshape(array, (tau_counter, 12) )
print("CHARGE SELECTOR is = ", CHARGE_SELECTOR)
print("The lenght of the array is ", len(array)," depending on CHARGE SELECTOR these are tau or fake tau")
print("The lenght of one tau of array is ", len(array[0])," and they should be 12 global variables")

if CHARGE_SELECTOR == 1:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datGlob_fake_tau_MC20.npy', array)
else:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datGlob_tau_MC20.npy', array)

'''#test if files and array coincide
J = 4
j = 0
input_file=ROOT.TFile.Open(path+'user.fmorodei.data15.periodE.v1.2022-01-18_histograms.root')
for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
    if j < J:
        print("in file ", evt.LeadTauTracksEta[0])
    j += 1
input_file.Close()
for j in range(J):
    print("in array ",array[50 + j])
'''
