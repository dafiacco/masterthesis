import ROOT
import glob
import numpy as np


#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/multijet/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'

file_counter = 0
tau_counter = 0
Fake_tau_counter = 0
CHARGE_SELECTOR = 1
LEPTON_SELECTOR = 0
array = []
tracks = []

print("In the directory there are ",len(glob.glob(path+"*histograms.root"))," root files")
for f in glob.glob(path+"*histograms.root"): #files:
    input_file=ROOT.TFile.Open(f) #path+f.name)
    file_counter += 1
    print("processing file: ", file_counter, "...")
    for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
        TauNAllTracks = evt.TauNAllTracks
        TauEta = evt.TauEta
        TauPt = evt.TauPt
        TauNTracks = evt.TauNTracks
        if CHARGE_SELECTOR == 1 :
            if len(TauEta) == 2 :
                if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                    EVENT_SELECTOR = 1
                    if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                        EVENT_SELECTOR = 2
                else :
                    EVENT_SELECTOR = 0 #It appens when one tau-filter is False
            else :
                EVENT_SELECTOR = 0 #It appens if we do't have two true tau
            if EVENT_SELECTOR == 2 and LEPTON_SELECTOR == 0: #It works on both fake-tau
                Fake_tau_counter += 2
                etOverPtLeadTrk = evt.etOverPtLeadTrk
                ipSigLeadTrk =evt.ipSigLeadTrk
                massTrkSys = evt.massTrkSys
                trFlightPathSig = evt.trFlightPathSig
                isolFrac = evt.isolFrac
                centFrac = evt.centFrac
                dRmax = evt.dRmax
                ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                array.append( [etOverPtLeadTrk[0], ipSigLeadTrk[0], massTrkSys[0], trFlightPathSig[0],
                                  isolFrac[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
                array.append( [etOverPtLeadTrk[1], ipSigLeadTrk[1], massTrkSys[1], trFlightPathSig[1], 
                                  isolFrac[1], centFrac[1], dRmax[1], ClustersMeanCenterLambda[1], ClustersMeanSecondLambda[1] ] )
            MuonCharge = evt.MuonCharge
            EleCharge = evt.EleCharge
            if LEPTON_SELECTOR == 1 and len(MuonCharge) > 0 and len(TauCharge) > 0 :
                if MuonCharge[0] == TauCharge[0]: ## work on fake tau of LH with L = Muon
                    Fake_tau_counter += 1
                    etOverPtLeadTrk = evt.etOverPtLeadTrk
                    ipSigLeadTrk =evt.ipSigLeadTrk
                    massTrkSys = evt.massTrkSys
                    trFlightPathSig = evt.trFlightPathSig
                    isolFrac = evt.isolFrac
                    centFrac = evt.centFrac
                    dRmax = evt.dRmax
                    ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                    ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                    array.append( [etOverPtLeadTrk[0], ipSigLeadTrk[0], massTrkSys[0], trFlightPathSig[0],
                                  isolFrac[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
            if LEPTON_SELECTOR == 1 and len(EleCharge) > 0 and len(TauCharge) > 0:
                if EleCharge[0] == TauCharge[0]: ## work on fake tau of LH with L = Ele
                    Fake_tau_counter += 1
                    etOverPtLeadTrk = evt.etOverPtLeadTrk
                    ipSigLeadTrk =evt.ipSigLeadTrk
                    massTrkSys = evt.massTrkSys
                    trFlightPathSig = evt.trFlightPathSig
                    isolFrac = evt.isolFrac
                    centFrac = evt.centFrac
                    dRmax = evt.dRmax
                    ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                    ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                    array.append( [etOverPtLeadTrk[0], ipSigLeadTrk[0], massTrkSys[0], trFlightPathSig[0],
                                  isolFrac[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
        else : #It enters here only if CHARGE_SELECTOR is off, and so works on true tau
            if len(TauEta) == 2 :
                if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                    EVENT_SELECTOR = 1
                    if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                        EVENT_SELECTOR = 2
                else :
                    EVENT_SELECTOR = 0 #It appens when one tau-filter is False
            else :
                EVENT_SELECTOR = 0 #It appens if we do't have two true tau
            if EVENT_SELECTOR == 2:#Lead and Sublead Tau
                tau_counter += 2
                etOverPtLeadTrk = evt.etOverPtLeadTrk
                ipSigLeadTrk =evt.ipSigLeadTrk
                massTrkSys = evt.massTrkSys
                trFlightPathSig = evt.trFlightPathSig
                isolFrac = evt.isolFrac
                centFrac = evt.centFrac
                dRmax = evt.dRmax
                ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                array.append( [etOverPtLeadTrk[0], ipSigLeadTrk[0], massTrkSys[0], trFlightPathSig[0],
                                  isolFrac[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
                array.append( [etOverPtLeadTrk[1], ipSigLeadTrk[1], massTrkSys[1], trFlightPathSig[1],
                                  isolFrac[1], centFrac[1], dRmax[1], ClustersMeanCenterLambda[1], ClustersMeanSecondLambda[1] ] )
    input_file.Close()
if CHARGE_SELECTOR == 1:
    array = np.reshape(array, (Fake_tau_counter, 9) )
else:
    array = np.reshape(array, (tau_counter, 9) )
print("CHARGE SELECTOR is = ", CHARGE_SELECTOR)
print("The lenght of the array is ", len(array)," depending on CHARGE SELECTOR these are tau or fake tau")
print("The lenght of one tau of array is ", len(array[0])," and they should be 9 global variables")

if CHARGE_SELECTOR == 1:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datGlob_fake_tau.npy', array)
else:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datGlob_tau.npy', array)

'''#test if files and array coincide
J = 4
j = 0
input_file=ROOT.TFile.Open(path+'user.fmorodei.data15.periodE.v1.2022-01-18_histograms.root')
for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
    if j < J:
        print("in file ", evt.LeadTauTracksEta[0])
    j += 1
input_file.Close()
for j in range(J):
    print("in array ",array[50 + j])
'''
