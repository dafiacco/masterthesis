from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import glob
import os
#import TauAnalysis.utils.utils_Zmumu.functions as fn 

#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/g/gpadovan/public/forDavide/'
path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/THOR_ntuples/'

#files = os.scandir(path = path)
file_counter = 0

###Definition oh histograms
xlow = 0
xup = 4
nbins = 4
h_NumTauPerEvent = ROOT.TH1F("NumTauPerEvent", "NumTauPerEvent", nbins, xlow, xup)
xlow = 0
xup = 8000
nbins = 70
h_CluMeanCentL = ROOT.TH1F("Clusters_Mean_Center_Lambda", "Clusters_Mean_Center_Lambda", nbins, xlow, xup) # set log y
xlow = 0
xup = 2400000
h_CluMeanSecL = ROOT.TH1F("Clusters_Mean_Second_Lambda", "Clusters_Mean_Second_Lambda", nbins, xlow, xup) # set log y
xlow = 0
xup = 0.4
h_dRmax = ROOT.TH1F("dRmax", "dRmax", nbins, xlow, xup) # set log y
xlow = -1
xup = 3
h_centFrac = ROOT.TH1F("centFrac", "centFrac", nbins, xlow, xup)# set log y
xlow = -1.5
xup = 2
#h_isolFrac = ROOT.TH1F("isolFrac", "isolFrac", nbins, xlow, xup)#set log y
xlow = -1500
xup = 1500
h_trFlightPathSig = ROOT.TH1F("trFlightPathSig", "trFlightPathSig", nbins, xlow, xup)# set log y
xlow = 0
xup = 40
h_massTrkSys = ROOT.TH1F("massTrkSys", "massTrkSys", nbins, xlow, xup)# set log y
xlow = 0
xup = 3.5
h_massTrkSys_zoom = ROOT.TH1F("massTrkSysZOOM", "massTrkSysZOOM", nbins, xlow, xup)# set log y
xlow = -400
xup = 350
#h_ipSigLeadTrk = ROOT.TH1F("ipSigLeadTrk", "ipSigLeadTrk", nbins, xlow, xup)# set log y
xlow = 0
xup = 65
h_etOverPtLeadTrk = ROOT.TH1F("etOverPtLeadTrkk", "etOverPtLeadTrk", nbins, xlow, xup)# set log y
xlow = 0
xup = 35
nbins = 35
#h_nVtx = ROOT.TH1F("nVtx", "nVtx", nbins, xlow, xup)

print("In the directory there are ",len(glob.glob(path+"Ztautau*.root"))," root files")
### Loop over the ntuples in the directory
for f in glob.glob(path+"Ztautau*.root"): #files:
    if f != 'naan':             #.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(f) #path+f.name)
        file_counter += 1
        print("processing file: ", file_counter, "...")
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            weight = 1 #evt.EventInfoAuxDyn.mcEventWeights #aggiungere altre variabili
            TauEta = evt.TauEta
            ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
            ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
            dRmax = evt.dRmax #the maximum dR between track associated with possibleTau and the estimated Tau direction
            centFrac = evt.centFrac #central energy fraction
            #isolFrac = evt.isolFrac # 
            massTrkSys = evt.massTrkSys #inv mass calculated by sum of 4-momentum of associated tracks (core + isolated), assuming a pion mass for each track
            trFlightPathSig = evt.trFlightPathSig #Decay lenght of secondary vertex in transfer plane, calculated respect tau vertex, divided by uncertainties
            #ipSigLeadTrk = evt.ipSigLeadTrk # Transverse impact parameter of leading track, calculated respect tau vertex, divided by its uncertainty
            etOverPtLeadTrk = evt.etOverPtLeadTrk #The transverce energy sum, calib at EM scale, deposited at core region of Topocluster, divided by pT of lead track 
            #LeadTauTracksPt = evt.LeadTauTracksPt
            #LeadTauTracksPhi = evt.LeadTauTracksPhi
            #LeadTauTracksE = evt.LeadTauTracksE
            if len(TauEta) != len(ClustersMeanCenterLambda):
                print("found a difference in lenght")
            h_NumTauPerEvent.Fill( len(ClustersMeanCenterLambda) )
            for i in range(len(ClustersMeanCenterLambda)):
                h_CluMeanCentL.Fill(ClustersMeanCenterLambda[i],weight)
                h_CluMeanSecL.Fill(ClustersMeanSecondLambda[i],weight)
                h_dRmax.Fill(dRmax[i],weight)
                h_centFrac.Fill(centFrac[i],weight)
                #h_isolFrac.Fill(isolFrac[i],weight)
                h_trFlightPathSig.Fill(trFlightPathSig[i],weight)
                h_massTrkSys.Fill(massTrkSys[i]/1000,weight)
                h_massTrkSys_zoom.Fill(massTrkSys[i]/1000,weight)
                #h_ipSigLeadTrk.Fill(ipSigLeadTrk[i],weight)
                h_etOverPtLeadTrk.Fill(etOverPtLeadTrk[i],weight)
        input_file.Close()
#files.close()

canv = ROOT.TCanvas("canvas","canvas1")
#ROOT.gStyle.SetOptStat(0) #toglie la tabella
h_NumTauPerEvent.GetXaxis().SetTitle("Number of Tau per Event")
h_NumTauPerEvent.GetYaxis().SetTitle("Events")
h_NumTauPerEvent.Draw("hist")
canv.SaveAs("histograms_mc20/NumTauPerEvent.png")

#h_nVtx.GetXaxis().SetTitle("Number of Verteces per Event")
#h_nVtx.GetYaxis().SetTitle("Events")
#h_nVtx.Draw("hist")
#canv.SaveAs("histograms_mc20/nVtx.png")

canv.SetLogy()

h_CluMeanCentL.GetXaxis().SetTitle("Cluster Depth (#lambda_{cluster})")
h_CluMeanCentL.GetYaxis().SetTitle("Events")
h_CluMeanCentL.Draw("hist")
canv.SaveAs("histograms_mc20/ClustersMeanCenterLambda.png")

h_CluMeanSecL.GetXaxis().SetTitle("Longitudinal Cluster Extention (<#lambda^{2}_{cluster}>)")
h_CluMeanSecL.GetYaxis().SetTitle("Events")
h_CluMeanSecL.Draw("hist")
canv.SaveAs("histograms_mc20/ClustersMeanSecondLambda.png")

h_dRmax.GetXaxis().SetTitle("dRmax")
h_dRmax.GetYaxis().SetTitle("Events")
h_dRmax.Draw("hist")
canv.SaveAs("histograms_mc20/dRmax.png")

h_centFrac.GetXaxis().SetTitle("Central energy fraction (f_{cent})")
h_centFrac.GetYaxis().SetTitle("Events")
h_centFrac.Draw("hist")
canv.SaveAs("histograms_mc20/centFrac.png")

#h_isolFrac.GetXaxis().SetTitle("Momentum fraction of isolation tracks (f_{iso}^{track})")
#h_isolFrac.GetYaxis().SetTitle("Events")
#h_isolFrac.Draw("hist")
#canv.SaveAs("histograms_mc20/isolFrac.png")

h_trFlightPathSig.GetXaxis().SetTitle("Transverse flight path significance (S_{T}^{flight})")
h_trFlightPathSig.GetYaxis().SetTitle("Events")
h_trFlightPathSig.Draw("hist")
canv.SaveAs("histograms_mc20/trFlightPathSig.png")

h_massTrkSys.GetXaxis().SetTitle("Invariant mass of the track system (m^{Track}) [GeV]")
h_massTrkSys.GetYaxis().SetTitle("Events")
h_massTrkSys.Draw("hist")
canv.SaveAs("histograms_mc20/massTrkSys.png")

h_massTrkSys_zoom.GetXaxis().SetTitle("Invariant mass of the track system (m^{Track}) [GeV]")
h_massTrkSys_zoom.GetYaxis().SetTitle("Events")
h_massTrkSys_zoom.Draw("hist")
canv.SaveAs("histograms_mc20/massTrkSys_zoom.png")

#h_ipSigLeadTrk.GetXaxis().SetTitle("Transverse impact parameter significance of the leading track (|S_{leadtrack}|)")
#h_ipSigLeadTrk.GetYaxis().SetTitle("Events")
#h_ipSigLeadTrk.Draw("hist")
#canv.SaveAs("histograms_mc20/ipSigLeadTrk.png")

h_etOverPtLeadTrk.GetXaxis().SetTitle("Inverse momentum fraction of the leading track (f_{leadtrack}^{-1})")
h_etOverPtLeadTrk.GetYaxis().SetTitle("Events")
h_etOverPtLeadTrk.Draw("hist")
canv.SaveAs("histograms_mc20/etOverPtLeadTrk.png")
'''
            p1 = ROOT.TLorentzVector()
            p2 = ROOT.TLorentzVector()
            if len(tauPt)>1:
                p1.SetPtEtaPhiE(tauPt[0],tauEta[0],tauPhi[0],tauE[0])
                p2.SetPtEtaPhiE(tauPt[1],tauEta[1],tauPhi[1],tauE[1])
                pZ = p1+p2
                h_mmc1.Fill(pZ.M()/1000,weight)
'''
