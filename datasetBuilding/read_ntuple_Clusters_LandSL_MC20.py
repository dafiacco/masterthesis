
from __future__ import print_function
import ROOT
import numpy as np
#from scipy.optimize import curve_fit
import glob
import os
#import TauAnalysis.utils.utils_Zmumu.functions as fn 

#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/THOR_ntuples/'

#files = os.scandir(path = path)
file_counter = 0

###Definition of histograms
xlow = 0
xup = 5
nbins = 5
#h_NumTrackLeadPerEvent = ROOT.TH1F("NumTrackLeadPerEvent", "NumTrackLeadPerEvent", nbins, xlow, xup)
#h_NumTrackSubleadPerEvent = ROOT.TH1F("NumSublrackLeadPerEvent", "NumTrackSubleadPerEvent", nbins, xlow, xup)
xlow = 0
xup = 200
nbins = 90
h_LeadTauClustersEt = ROOT.TH1F("LeadTauClustersEt", "LeadTauClustersEt", nbins, xlow, xup) # set log y
xup = 180
h_SubleadTauClustersEt = ROOT.TH1F("SubleadTauClustersEt", "SubleadTauClustersEt", nbins, xlow, xup) # set log y
xlow = 0
xup = 700
h_LeadTauClustersE = ROOT.TH1F("LeadTauClustersE", "LeadTauClustersE", nbins, xlow, xup) # set log y
xup = 250
h_SubleadTauClustersE = ROOT.TH1F("SubleadTauClustersE", "SubleadTauClustersE", nbins, xlow, xup) # set log y
xlow = -4
xup = 4
nbins = 70
h_LeadTauClustersPhi = ROOT.TH1F("LeadTauClustersPhi", "LeadTauClustersPhi", nbins, xlow, xup)
h_SubleadTauClustersPhi = ROOT.TH1F("SubleadTauClustersPhi", "SubleadTauClustersPhi", nbins, xlow, xup)
xlow = -3.1
xup = 3.1
h_LeadTauClustersEta = ROOT.TH1F("LeadTauClustersEta", "LeadTauClustersEta", nbins, xlow, xup)
h_SubleadTauClustersEta = ROOT.TH1F("SubleadTauClustersEta", "SubleadTauClustersEta", nbins, xlow, xup)
xlow = -3
xup = 3
#h_LeadTauAllTracksEta = ROOT.TH1F("LeadTauAllTracksEta", "LeadTauAllTracksEta", nbins, xlow, xup)
xlow = -4
xup = 4
#h_LeadTauAllTracksPhi = ROOT.TH1F("LeadTauAllTracksPhi", "LeadTauAllTracksPhi", nbins, xlow, xup)
xlow = 0
xup = 5000000
#h_LeadTauAllTracksE = ROOT.TH1F("LeadTauAllTracksE", "LeadTauAllTracksE", nbins, xlow, xup)# set log y
xlow = 0
xup = 5000000
#h_LeadTauAllTracksPt = ROOT.TH1F("LeadTauAllTracksPt", "LeadTauAllTracksPt", nbins, xlow, xup)# set log y
xlow = -3
xup = 3
#h_SubleadTauAllTracksEta = ROOT.TH1F("SubleadTauAllTracksEta", "SubleadTauAllTracksEta", nbins, xlow, xup)
xlow = -4
xup = 4
#h_SubleadTauAllTracksPhi = ROOT.TH1F("SubleadTauAllTracksPhi", "SubleadTauAllTracksPhi", nbins, xlow, xup)
xlow = 0
xup = 5000000
#h_SubleadTauAllTracksE = ROOT.TH1F("SubleadTauAllTracksE", "SubleadTauAllTracksE", nbins, xlow, xup)# set log y
xlow = 0
xup = 5000000
#h_SubleadTauAllTracksPt = ROOT.TH1F("SubleadTauAllTracksPt", "SubleadTauAllTracksPt", nbins, xlow, xup)# set log y


print("In the directory there are ",len(glob.glob(path+"*.root"))," root files")
Entries = 0
FakeEntry = 0
### Loop over the ntuples in the directory
for f in glob.glob(path+"Ztautau*.root"): #files: 2022-01-20_histograms.root
    if f != 'naan':             #.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(f) #path+f.name)
        file_counter += 1
        print("processing file: ", file_counter, "...")
        Entries += input_file.T_Zp2thh_NOMINAL.GetEntries()
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            weight = 1#evt.mcWeight #aggiungere altre variabili
            #TauCharge = evt.TauCharge
            TauEta = evt.TauEta
            TauPhi = evt.TauPhi
            TauNTracks = evt.TauNTracks
            #if len(TauCharge) > 1 :
            #    if TauCharge[0] == TauCharge[1] :
            #        FakeEntry += 1
            LeadTauClustersEt = evt.LeadTauClusterEt
            LeadTauClustersE = evt.LeadTauClusterE
            LeadTauClustersPhi = evt.LeadTauClusterPhi
            LeadTauClustersEta = evt.LeadTauClusterEta
            SubleadTauClustersEta = evt.SubleadTauClusterEta
            SubleadTauClustersPhi = evt.SubleadTauClusterPhi
            SubleadTauClustersEt = evt.SubleadTauClusterEt
            SubleadTauClustersE = evt.SubleadTauClusterE
            #h_NumTrackLeadPerEvent.Fill( len(LeadTauTracksPt) )
            for i in range( len(LeadTauClustersEt) ):
                h_LeadTauClustersEt.Fill(LeadTauClustersEt[i]/1000,weight)
                h_LeadTauClustersE.Fill(LeadTauClustersE[i]/1000,weight)
                h_LeadTauClustersPhi.Fill(LeadTauClustersPhi[i],weight)
                h_LeadTauClustersEta.Fill(LeadTauClustersEta[i],weight)
            for i in range( len(SubleadTauClustersEt) ):
                h_SubleadTauClustersEta.Fill(SubleadTauClustersEta[i],weight)
                h_SubleadTauClustersPhi.Fill(SubleadTauClustersPhi[i],weight)
                h_SubleadTauClustersEt.Fill(SubleadTauClustersEt[i]/1000,weight)
                h_SubleadTauClustersE.Fill(SubleadTauClustersE[i]/1000,weight)
            #h_NumTrackSubleadPerEvent.Fill( len(SubleadTauTracksPt) )
        input_file.Close()
#files.close()
print("In the directory there are ", Entries," Total Entries.")
print("In the directory there are ", FakeEntry ," Fake Entries in total.")

canv = ROOT.TCanvas("canvas","canvas1")
#ROOT.gStyle.SetOptStat(0) #toglie la tabella
"""
h_NumTrackLeadPerEvent.GetXaxis().SetTitle("Number of Tracks in Leading Tau")
h_NumTrackLeadPerEvent.GetYaxis().SetTitle("Events")
h_NumTrackLeadPerEvent.Draw("hist")
canv.SaveAs("histograms_MC20/NumTrackLeadPerEvent.png")

h_NumTrackSubleadPerEvent.GetXaxis().SetTitle("Number of Tracks in Sub-leading Tau")
h_NumTrackSubleadPerEvent.GetYaxis().SetTitle("Events")
h_NumTrackSubleadPerEvent.Draw("hist")
canv.SaveAs("histograms_MC20/NumTrackSubleadPerEvent.png")
"""

h_LeadTauClustersPhi.GetXaxis().SetTitle("Phi of Clusters of Leading Tau")
h_LeadTauClustersPhi.GetYaxis().SetTitle("Events")
h_LeadTauClustersPhi.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauClustersPhi.png")

h_SubleadTauClustersPhi.GetXaxis().SetTitle("Phi of Clusters of Sub-leading Tau")
h_SubleadTauClustersPhi.GetYaxis().SetTitle("Events")
h_SubleadTauClustersPhi.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauClustersPhi.png")

h_LeadTauClustersEta.GetXaxis().SetTitle("Eta of Clusters of Leading Tau")
h_LeadTauClustersEta.GetYaxis().SetTitle("Events")
h_LeadTauClustersEta.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauClustersEta.png")

h_SubleadTauClustersEta.GetXaxis().SetTitle("Eta of Clusters of Sub-leading Tau")
h_SubleadTauClustersEta.GetYaxis().SetTitle("Events")
h_SubleadTauClustersEta.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauClustersEta.png")
"""
h_LeadTauAllTracksEta.GetXaxis().SetTitle("Eta of All Tracks of Leading Tau")
h_LeadTauAllTracksEta.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksEta.png")

h_LeadTauAllTracksPhi.GetXaxis().SetTitle("Phi of All Tracks of Leading Tau")
h_LeadTauAllTracksPhi.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksPhi.png")

h_SubleadTauAllTracksEta.GetXaxis().SetTitle("Eta of All Tracks of Sub-leading Tau")
h_SubleadTauAllTracksEta.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksEta.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksEta.png")

h_SubleadTauAllTracksPhi.GetXaxis().SetTitle("Phi of All Tracks of Sub-leading Tau")
h_SubleadTauAllTracksPhi.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksPhi.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksPhi.png")
"""
canv.SetLogy()

h_LeadTauClustersEt.GetXaxis().SetTitle("Et of Clusters of Leading Tau [GeV]")
h_LeadTauClustersEt.GetYaxis().SetTitle("Events")
h_LeadTauClustersEt.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauClustersEt.png")

h_SubleadTauClustersEt.GetXaxis().SetTitle("Et of Clusters of Sub-leading Tau [GeV]")
h_SubleadTauClustersEt.GetYaxis().SetTitle("Events")
h_SubleadTauClustersEt.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauClustersEt.png")

h_LeadTauClustersE.GetXaxis().SetTitle("Energy of Cluster of Leading Tau [GeV]")
h_LeadTauClustersE.GetYaxis().SetTitle("Events")
h_LeadTauClustersE.Draw("hist")
canv.SaveAs("histograms_mc20/LeadTauClustersE.png")

h_SubleadTauClustersE.GetXaxis().SetTitle("Energy of  Clusters Sub-leading Tau [GeV]")
h_SubleadTauClustersE.GetYaxis().SetTitle("Events")
h_SubleadTauClustersE.Draw("hist")
canv.SaveAs("histograms_mc20/SubleadTauClustersE.png")
"""
h_LeadTauAllTracksPt.GetXaxis().SetTitle("Pt of All Tracks Leading Tau [GeV]")
h_LeadTauAllTracksPt.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksPt.png")

h_LeadTauAllTracksE.GetXaxis().SetTitle("Energy of All Tracks of Leading Tau [GeV]")
h_LeadTauAllTracksE.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/LeadTauAllTracksE.png")

h_SubleadTauAllTracksPt.GetXaxis().SetTitle("Pt of All Tracks of Sub-leading Tau [GeV]")
h_SubleadTauAllTracksPt.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksPt.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksPt.png")

h_SubleadTauAllTracksE.GetXaxis().SetTitle("Energy of All Tracks of Sub-leading Tau [GeV]")
h_SubleadTauAllTracksE.GetYaxis().SetTitle("Events")
h_SubleadTauAllTracksE.Draw("hist")
canv.SaveAs("histograms_MC20/SubleadTauAllTracksE.png")
"""
