from __future__ import print_function
import ROOT
import math
#from scipy.optimize import curve_fit
import os
import glob
#import TauAnalysis.utils.utils_Zmumu.functions as fn


#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'

#files = os.scandir(path = path)

file_counter = 0

def angle_dist(tauEta, tauPhi, tracksEta, tracksPhi):
    deltaEta = []
    for i in range(len(tracksEta)):
        deltaEta.append(tauEta - tracksEta[i])
    deltaPhi = []
    for i in range(len(tracksPhi)):
        deltaPhi.append( ROOT.TVector2.Phi_mpi_pi( tauPhi - tracksPhi[i] ) )
    return deltaEta, deltaPhi

##Definition of histograms
xlow=-0.5
xup=0.5
nbins = 70
h_LeadTauTracksDeltaEta = ROOT.TH1F("LeadTauTracksDeltaEta", "LeadTauTracksDeltaEta", nbins, xlow, xup)
xlow = -0.7
xup = 0.7
h_LeadTauTracksIsolationDeltaEta = ROOT.TH1F("LeadTauTracksIsolationDeltaEta", "LeadTauTracksIsolationDeltaEta", nbins, xlow, xup)
h_LeadTauOtherTracksDeltaEta = ROOT.TH1F("LeadTauOtherTracksDeltaEta", "LeadTauOtherTracksDeltaEta", nbins, xlow, xup)
xlow = -0.5
xup = 0.5
h_LeadTauTracksDeltaPhi = ROOT.TH1F("LeadTauTracksDeltaPhi", "LeadTauTracksDeltaPhi", nbins, xlow, xup)
xlow = -0.7
xup = 0.7
h_LeadTauTracksIsolationDeltaPhi = ROOT.TH1F("LeadTauTracksIsolationDeltaPhi", "LeadTauTracksIsolationDeltaPhi", nbins, xlow, xup)
h_LeadTauOtherTracksDeltaPhi = ROOT.TH1F("LeadTauOtherTracksDeltaPhi", "LeadTauOtherTracksDeltaPhi", nbins, xlow, xup)
xlow = 0
xup = 0.2
h_LeadTauTracksDelraR = ROOT.TH1F("LeadTauTracksDelraR", "LeadTauTracksDelraR", nbins, xlow, xup)
xlow = 0
xup = 0.5
h_LeadTauTracksIsolationDelraR = ROOT.TH1F("LeadTauTracksIsolationDelraR", "LeadTauTracksIsolationDelraR", nbins, xlow, xup)
xlow = 0
xup = 0.6
h_LeadTauAllTracksDelraR = ROOT.TH1F("LeadTauAllTracksDelraR", "LeadTauAllTracksDelraR", nbins, xlow, xup)
h_LeadTauOtherTracksDelraR = ROOT.TH1F("LeadTauOtherTracksDelraR", "LeadTauOtherTracksDelraR",  nbins, xlow, xup)

#output_file=ROOT.TFile.Open('/afs/cern.ch/user/d/dafiacco/eos/analysis/histograms/TTree_output.root')
print("In the directory there are ",len(glob.glob(path+"*2022-01-20_histograms.root"))," root files")
### Loop over the ntuples in the directory
Event_counter = 0
for f in glob.glob(path+"*2022-01-20_histograms.root"): #files:
    if f != 'naan':             #.is_file(): ## check if it's a file and not a directory
        input_file=ROOT.TFile.Open(f)
        #input_file=ROOT.TFile.Open(path)
        file_counter += 1
        print("processing file: ", file_counter, "...")
        if file_counter > 0:
            for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
                Event_counter += 1
                weight = evt.mcWeight #aggiungere altre variabili
                TauEta = evt.TauEta
                TauPhi = evt.TauPhi
                LeadTauTracksEta = evt.LeadTauTracksEta
                LeadTauTracksPhi = evt.LeadTauTracksPhi
                LeadTauTracksIsolationEta = evt.LeadTauTracksIsolationEta
                LeadTauTracksIsolationPhi = evt.LeadTauTracksIsolationPhi
                LeadTauAllTracksEta = evt.LeadTauAllTracksEta
                LeadTauAllTracksPhi = evt.LeadTauAllTracksPhi
                if len(TauEta) > 0:
                    print("LeadTauTracksEta = ",LeadTauTracksEta)
                    LeadTauTracksDeltaEta, LeadTauTracksDeltaPhi = angle_dist( TauEta[0], TauPhi[0], LeadTauTracksEta, LeadTauTracksPhi)
                    print("LeadTauTracksDeltaEta = ",LeadTauTracksDeltaEta)
                    for i in range( len(LeadTauTracksDeltaEta) ): ## core tracks
                        h_LeadTauTracksDeltaEta.Fill(LeadTauTracksDeltaEta[i],weight)
                        h_LeadTauTracksDeltaPhi.Fill(LeadTauTracksDeltaPhi[i],weight)
                        DeltaR = math.sqrt( LeadTauTracksDeltaEta[i]**2 + LeadTauTracksDeltaPhi[i]**2 )
                        print("DeltaR = ",DeltaR)
                        h_LeadTauTracksDelraR.Fill(DeltaR,weight)
                    LeadTauTracksIsolationDeltaEta, LeadTauTracksIsolationDeltaPhi = angle_dist( TauEta[0], TauPhi[0], LeadTauTracksIsolationEta, LeadTauTracksIsolationPhi)
                    print("LeadTauTracksIsolationDeltaEta = ",LeadTauTracksIsolationDeltaEta)
                    for i in range( len(LeadTauTracksIsolationDeltaEta) ): ## Isolation tracks
                        h_LeadTauTracksIsolationDeltaEta.Fill(LeadTauTracksIsolationDeltaEta[i],weight)
                        h_LeadTauTracksIsolationDeltaPhi.Fill(LeadTauTracksIsolationDeltaPhi[i],weight)
                        DeltaR = math.sqrt( LeadTauTracksIsolationDeltaEta[i]**2 + LeadTauTracksIsolationDeltaPhi[i]**2 )
                        print("IsolationDeltaR = ",DeltaR)
                        h_LeadTauTracksIsolationDelraR.Fill(DeltaR,weight)
                    LeadTauAllTracksDeltaEta, LeadTauAllTracksDeltaPhi = angle_dist( TauEta[0], TauPhi[0], LeadTauAllTracksEta, LeadTauAllTracksPhi)
                    len_corePisol = len(LeadTauTracksDeltaEta) + len(LeadTauTracksIsolationDeltaEta)
                    for i in range( len(LeadTauAllTracksDeltaEta) ): ## All tracks
                        DeltaR = math.sqrt( LeadTauAllTracksDeltaEta[i]**2 + LeadTauAllTracksDeltaPhi[i]**2 )
                        h_LeadTauAllTracksDelraR.Fill(DeltaR,weight)
                    for i in range( len(LeadTauAllTracksDeltaEta)  - len_corePisol ): ## Other tracks
                        h_LeadTauOtherTracksDeltaEta.Fill(LeadTauAllTracksDeltaEta[i+len_corePisol],weight)
                        h_LeadTauOtherTracksDeltaPhi.Fill(LeadTauAllTracksDeltaPhi[i+len_corePisol],weight)
                        DeltaR = math.sqrt( LeadTauAllTracksDeltaEta[i+len_corePisol]**2 + LeadTauAllTracksDeltaPhi[i+len_corePisol]**2 )
                        h_LeadTauOtherTracksDelraR.Fill(DeltaR,weight)
        input_file.Close()
#files.close()


canv = ROOT.TCanvas("canvas","canvas1")

h_LeadTauTracksDeltaPhi.GetXaxis().SetTitle("DeltaPhi of core tracks of Leading Tau")
h_LeadTauTracksDeltaPhi.GetYaxis().SetTitle("Events")
h_LeadTauTracksDeltaPhi.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksDeltaPhi.png")

h_LeadTauTracksIsolationDeltaPhi.GetXaxis().SetTitle("DeltaPhi of Isolation tracks of Leading Tau")
h_LeadTauTracksIsolationDeltaPhi.GetYaxis().SetTitle("Events")
h_LeadTauTracksIsolationDeltaPhi.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksIsolationDeltaPhi.png")

h_LeadTauOtherTracksDeltaPhi.GetXaxis().SetTitle("DeltaPhi of Other tracks of Leading Tau")
h_LeadTauOtherTracksDeltaPhi.GetYaxis().SetTitle("Events")
h_LeadTauOtherTracksDeltaPhi.Draw("hist")
canv.SaveAs("histograms/LeadTauOtherTracksDeltaPhi.png")

h_LeadTauTracksDeltaEta.GetXaxis().SetTitle("DeltaEta of core tracks of Leading Tau")
h_LeadTauTracksDeltaEta.GetYaxis().SetTitle("Events")
h_LeadTauTracksDeltaEta.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksDeltaEta.png")

h_LeadTauTracksIsolationDeltaEta.GetXaxis().SetTitle("DeltaEta of Isolation tracks of Leading Tau")
h_LeadTauTracksIsolationDeltaEta.GetYaxis().SetTitle("Events")
h_LeadTauTracksIsolationDeltaEta.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksIsolationDeltaEta.png")

h_LeadTauOtherTracksDeltaEta.GetXaxis().SetTitle("DeltaEta of Other tracks of Leading Tau")
h_LeadTauOtherTracksDeltaEta.GetYaxis().SetTitle("Events")
h_LeadTauOtherTracksDeltaEta.Draw("hist")
canv.SaveAs("histograms/LeadTauOtherTracksDeltaEta.png")

h_LeadTauTracksDelraR.GetXaxis().SetTitle("DeltaR of core tracks of Leading Tau")
h_LeadTauTracksDelraR.GetYaxis().SetTitle("Events")
h_LeadTauTracksDelraR.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksDeltaR.png")

h_LeadTauTracksIsolationDelraR.GetXaxis().SetTitle("DeltaR of Isolation tracks of Leading Tau")
h_LeadTauTracksIsolationDelraR.GetYaxis().SetTitle("Events")
h_LeadTauTracksIsolationDelraR.Draw("hist")
canv.SaveAs("histograms/LeadTauTracksIsolationDeltaR.png")

h_LeadTauAllTracksDelraR.GetXaxis().SetTitle("DeltaR of All Tracks of Leading Tau")
h_LeadTauAllTracksDelraR.GetYaxis().SetTitle("Events")
h_LeadTauAllTracksDelraR.Draw("hist")
canv.SaveAs("histograms/LeadTauAllTracksDeltaR.png")

h_LeadTauOtherTracksDelraR.GetXaxis().SetTitle("DeltaR of Other Tracks of Leading Tau")
h_LeadTauOtherTracksDelraR.GetYaxis().SetTitle("Events")
h_LeadTauOtherTracksDelraR.Draw("hist")
canv.SaveAs("histograms/LeadTauOtherTracksDeltaR.png")
'''
canv = ROOT.TCanvas("canvas","canvas1")
pad1 = ROOT.TPad("pad1","tau mass", 0.02, 0.51, 0.98, 0.98)
pad2 = ROOT.TPad("pad2","tau eta", 0.02, 0.02, 0.98, 0.49)
pad1.Draw()
pad2.Draw()

#ROOT.gStyle.SetOptStat(0)
pad1.cd()
h_muh_mtmc.GetXaxis().SetTitle("mT [GeV]")
h_muh_mtmc.GetYaxis().SetTitle("Events")
h_muh_mtmc.Draw("hist")
canv.Update()

pad2.cd()
h_eh_mtmc.GetXaxis().SetTitle("mT [GeV]")
h_eh_mtmc.GetYaxis().SetTitle("Events")
h_eh_mtmc.Draw("hist")
canv.Update()

canv.SaveAs("histograms/tau_LH_mT.png")
'''
