import ROOT
import glob
import numpy as np
import sys
import string

#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/multijet/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/THOR_ntuples/'
#path = '/eos/user/f/fmorodei/public/ntuple4Davide/THOR_ntuples/Ztautau/'
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'
path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/new_download/'

file_counter = 0
tau_counter = 0
Fake_tau_counter = 0
event_counter = 0
CHARGE_SELECTOR = 1
LEPTON_SELECTOR = 0
array = []
Glob_array = []

def track_appender (array, DEta, DPhi, Pt, d0, z0sintheta, N_IBP, N_Pixels, N_SCT):
    J = 20 - len(DEta)
    if J < 0:
        for i in range(20) :
            array.append([DEta[i], DPhi[i], Pt[i], 0., 0., d0[i], z0sintheta[i], ord(N_IBP[i]), ord(N_Pixels[i]), ord(N_SCT[i]), 0., 0., 0., 1.])
    else:
        for i in range(len(DEta)):
            array.append([DEta[i], DPhi[i], Pt[i], 0., 0., d0[i], z0sintheta[i], ord(N_IBP[i]), ord(N_Pixels[i]), ord(N_SCT[i]), 0., 0., 0., 1.])
        for i in range(J) :
            array.append([-9, -9, -999, 0., 0., -999, -999,  -999, -999, -999, 0., 0., 0., -9])
    
def cluster_appender (array, DEta, DPhi, Et, E, lamb, lamb2, r2):
    J = 30 - len(DEta)
    if J < 0:
        for i in range(30) :
            array.append([DEta[i], DPhi[i], 0., Et[i], E[i], 0., 0., 0., 0., 0., lamb[i], lamb2[i], r2[i], 5.])
    else:
        for i in range(len(DEta)):
            array.append([DEta[i], DPhi[i], 0., Et[i], E[i], 0., 0., 0., 0., 0., lamb[i], lamb2[i], r2[i], 5.])
        for i in range(J) :
            array.append([-9, -9, 0., -999, -999, 0., 0., 0., 0., 0., -999, -999, -999, -10])


print("In the directory there are ",len(glob.glob(path+"*.root"))," root files")
for f in glob.glob(path+"*.root"): #files:
    input_file=ROOT.TFile.Open(f) #path+f.name)
    file_counter += 1
    print("processing file: ", file_counter, "...")
    if file_counter < 55 :
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            event_counter += 1
            #TauNAllTracks = evt.TauNAllTracks
            TauEta = evt.TauEta
            #TauPhi = evt.TauPhi
            TauPt = evt.TauPt
            TauNTracks = evt.TauNTracks
            TauIsTruthMatched = evt.TauIsTruthMatched
            if CHARGE_SELECTOR == 1:#( CHARGE_SELECTOR == 0 and TauIsTruthMatched[0] == '\x01' and TauIsTruthMatched[1] == '\x01') #It works on QCD events or on Tau events, depending on CS value
                if len(TauEta) == 2 and (event_counter >= int(sys.argv[1]) and  event_counter < int(sys.argv[2]) ):
                    if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                        EVENT_SELECTOR = 1
                        if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                            EVENT_SELECTOR = 2
                    else :
                        EVENT_SELECTOR = 0 #It appens when one tau-filter is False
                else :
                    EVENT_SELECTOR = 0 #It appens if we do't have two true tau
                if EVENT_SELECTOR == 2:#Lead and Sublead Tau
                    Fake_tau_counter += 2
                    tau_counter += 2
                    TauNTracksIsolation = evt.TauNTracksIsolation ### Here works on tracks
                    LeadTauTracks_dEta = evt.LeadTauTracks_dEta
                    LeadTauTracks_dPhi = evt.LeadTauTracks_dPhi
                    LeadTauTracksPt = evt.LeadTauTracksPt
                    LeadTauTracks_d0 = evt.LeadTauTracks_d0
                    LeadTauTracks_z0sintheta = evt.LeadTauTracks_z0sintheta
                    LeadTauTracks_nIBPHits = evt.LeadTauTracks_nInnermostPixelHits
                    LeadTauTracks_nPixelHits = evt.LeadTauTracks_nPixelHits
                    LeadTauTracks_nSCTHits = evt.LeadTauTracks_nSCTHits
                    track_appender( array,  LeadTauTracks_dEta, LeadTauTracks_dPhi, LeadTauTracksPt, LeadTauTracks_d0, LeadTauTracks_z0sintheta, 
                               LeadTauTracks_nIBPHits, LeadTauTracks_nPixelHits, LeadTauTracks_nSCTHits)
                    LeadTauCluster_dEta = evt.LeadTauCluster_dEta ### Here works on Clusters
                    LeadTauCluster_dPhi = evt.LeadTauCluster_dPhi
                    LeadTauClusterEt = evt.LeadTauClusterEt
                    LeadTauClusterE = evt.LeadTauClusterE
                    LeadTauCluster_center_lambda = evt.LeadTauCluster_center_lambda
                    LeadTauCluster_second_lambda = evt.LeadTauCluster_second_lambda
                    LeadTauCluster_second_R = evt.LeadTauCluster_second_R
                    cluster_appender( array,  LeadTauCluster_dEta, LeadTauCluster_dPhi, LeadTauClusterEt, LeadTauClusterE, LeadTauCluster_center_lambda, LeadTauCluster_second_lambda,
                               LeadTauCluster_second_R)
                    SubleadTauTracks_dEta = evt.SubleadTauTracks_dEta
                    SubleadTauTracks_dPhi = evt.SubleadTauTracks_dPhi
                    SubleadTauTracksPt = evt.SubleadTauTracksPt
                    SubleadTauTracks_d0 = evt.SubleadTauTracks_d0
                    SubleadTauTracks_z0sintheta = evt.SubleadTauTracks_z0sintheta
                    SubleadTauTracks_nIBPHits = evt.SubleadTauTracks_nInnermostPixelHits
                    SubleadTauTracks_nPixelHits = evt.SubleadTauTracks_nPixelHits
                    SubleadTauTracks_nSCTHits = evt.SubleadTauTracks_nSCTHits
                    track_appender( array, SubleadTauTracks_dEta, SubleadTauTracks_dPhi, SubleadTauTracksPt, SubleadTauTracks_d0, SubleadTauTracks_z0sintheta,
                                SubleadTauTracks_nIBPHits, SubleadTauTracks_nPixelHits, SubleadTauTracks_nSCTHits)
                    SubleadTauCluster_dEta = evt.SubleadTauCluster_dEta
                    SubleadTauCluster_dPhi = evt.SubleadTauCluster_dPhi
                    SubleadTauClusterEt = evt.SubleadTauClusterEt
                    SubleadTauClusterE = evt.SubleadTauClusterE
                    SubleadTauCluster_center_lambda = evt.SubleadTauCluster_center_lambda
                    SubleadTauCluster_second_lambda = evt.SubleadTauCluster_second_lambda
                    SubleadTauCluster_second_R = evt.SubleadTauCluster_second_R
                    cluster_appender( array,  SubleadTauCluster_dEta, SubleadTauCluster_dPhi, SubleadTauClusterEt, SubleadTauClusterE, SubleadTauCluster_center_lambda, SubleadTauCluster_second_lambda,
                               SubleadTauCluster_second_R)
                    TauPt = evt.TauPt
                    TauE = evt.TauE
                    TauNTracks = evt.TauNTracks
                    TauNTracksIsolation = evt.TauNTracksIsolation
                    TauNClusters = evt.TauNClusters
                    TauSeedJetPt = evt.TauSeedJetPt
                    etOverPtLeadTrk = evt.etOverPtLeadTrk
                    massTrkSys = evt.massTrkSys
                    trFlightPathSig = evt.trFlightPathSig
                    centFrac = evt.centFrac
                    dRmax = evt.dRmax
                    ClustersMeanCenterLambda = evt.ClustersMeanCenterLambda
                    ClustersMeanSecondLambda = evt.ClustersMeanSecondLambda
                    Glob_array.append( [TauPt[0], TauE[0], TauNTracks[0], TauNTracksIsolation[0], TauNClusters[0], TauSeedJetPt[0], etOverPtLeadTrk[0],
                                  massTrkSys[0], trFlightPathSig[0], centFrac[0], dRmax[0], ClustersMeanCenterLambda[0], ClustersMeanSecondLambda[0] ] )
                    Glob_array.append( [TauPt[1], TauE[1], TauNTracks[1], TauNTracksIsolation[1], TauNClusters[1], TauSeedJetPt[1], etOverPtLeadTrk[1],
                                  massTrkSys[1], trFlightPathSig[1], centFrac[1], dRmax[1], ClustersMeanCenterLambda[1], ClustersMeanSecondLambda[1] ] )
    input_file.Close()
if CHARGE_SELECTOR == 1:
    array = np.reshape(array, (Fake_tau_counter, 50, 14) )
    Glob_array = np.reshape(Glob_array, (Fake_tau_counter, 13) )
else:
    array = np.reshape(array, (tau_counter, 50, 14 ) )
    Glob_array = np.reshape(Glob_array, (tau_counter, 13) )
print("CHARGE SELECTOR is = ", CHARGE_SELECTOR)
print("The lenght of the array is ", len(array)," depending on CHARGE SELECTOR these are tau or fake tau")
print("The lenght of one tau of array is ", len(array[0])," and they should be 20 tracks and 30 clusters")
print("The lenght of one tau of array is ", len(array[0][0])," and they are: DEta, DPhi, Pt(trk), Et(cls), E(cls), d0(trk), z0*sintheta(trk), N_IBP(trk), N_Pixel(trk), N_SCT(trk), lambda(cls), lambda2(cls), r2(cls),  Flag") #on track-flag I'm not so sure
print("The lenght of the Glob_array is ", len(Glob_array)," depending on CHARGE SELECTOR these are tau or fake tau")
print("The lenght of one tau of Glob_array is ", len(Glob_array[0])," and they should be 13 global variables")
if CHARGE_SELECTOR == 1:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datasetMC20/dataset_fake_tau_MC20_{0}_{1}.npy'.format(sys.argv[1], sys.argv[2]), array)
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datasetMC20/datGlob_fake_tau_MC20_{0}_{1}.npy'.format(sys.argv[1], sys.argv[2]), Glob_array)
else:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datasetMC20/dataset_tau_MC20_{0}_{1}.npy'.format(sys.argv[1], sys.argv[2]), array)
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/datasetMC20/datGlob_tau_MC20_{0}_{1}.npy'.format(sys.argv[1], sys.argv[2]), Glob_array)
