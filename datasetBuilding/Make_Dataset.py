import ROOT
import glob
import numpy as np

#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/multijet/'
#path = '/eos/user/f/fmorodei/PhD/ntuple_per_Davide/Ztautau/'
path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/multijet/'

file_counter = 0
tau_counter = 0
Fake_tau_counter = 0
CHARGE_SELECTOR = 1
LEPTON_SELECTOR = 0
array = []
tracks = []

def angle_dist(tauEta, tauPhi, tracksEta, tracksPhi):
    deltaEta = []
    for i in range(len(tracksEta)):
        deltaEta.append(tauEta - tracksEta[i])
    deltaPhi = []
    for i in range(len(tracksPhi)):
        deltaPhi.append( ROOT.TVector2.Phi_mpi_pi( tauPhi - tracksPhi[i] ) )
    return deltaEta, deltaPhi

def appender (array, DEta, DPhi, Pt, E, d0, z0, theta, NCoreTracks, NIsolTracks):
    J = 10 - len(DEta)
    if J < 0:
        for i in range(10) :
            if i < NCoreTracks:
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 1.])
            elif i < NCoreTracks+NIsolTracks :
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 2.])
            else:
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 3.])
    else:
        for i in range(len(DEta)):
            if i < NCoreTracks:
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 1.])
            elif i < NCoreTracks+NIsolTracks :
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 2.])
            else:
                array.append([DEta[i], DPhi[i], Pt[i], E[i], d0[i], z0[i], theta[i], 3.])
        for i in range(J) :
            array.append([-9, -9, -999, -999, -999, -999, -999, -9])
    
    

print("In the directory there are ",len(glob.glob(path+"*histograms.root"))," root files")
for f in glob.glob(path+"*histograms.root"): #files:
    input_file=ROOT.TFile.Open(f) #path+f.name)
    file_counter += 1
    print("processing file: ", file_counter, "...")
    if file_counter < 55 :
        for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
            TauNAllTracks = evt.TauNAllTracks
            TauEta = evt.TauEta
            TauPhi = evt.TauPhi
            TauPt = evt.TauPt
            TauNTracks = evt.TauNTracks
            if CHARGE_SELECTOR == 1 :#It works on QCD events
                if len(TauEta) == 2 :
                    if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                        EVENT_SELECTOR = 1
                        if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                            EVENT_SELECTOR = 2
                    else :
                        EVENT_SELECTOR = 0 #It appens when one tau-filter is False
                else :
                    EVENT_SELECTOR = 0 #It appens if we do't have two true tau
                if EVENT_SELECTOR == 2 and LEPTON_SELECTOR == 0: #It works on both fake-tau
                    Fake_tau_counter += 2
                    TauNTracks = evt.TauNTracks
                    TauNTracksIsolation = evt.TauNTracksIsolation
                    LeadTauAllTracksEta = evt.LeadTauAllTracksEta
                    LeadTauAllTracksPhi = evt.LeadTauAllTracksPhi
                    LeadTauAllTracksPt = evt.LeadTauAllTracksPt
                    LeadTauAllTracksE = evt.LeadTauAllTracksE
                    LeadTauAllTracks_d0 = evt.LeadTauAllTracks_d0
                    LeadTauAllTracks_z0 = evt.LeadTauAllTracks_z0
                    LeadTauAllTracksTheta = evt.LeadTauAllTracksTheta
                    LeadTauAllTracksDeltaEta, LeadTauAllTracksDeltaPhi = angle_dist( TauEta[0], TauPhi[0], LeadTauAllTracksEta, LeadTauAllTracksPhi )
                    appender( array,  LeadTauAllTracksDeltaEta, LeadTauAllTracksDeltaPhi, LeadTauAllTracksPt, LeadTauAllTracksE,
                             LeadTauAllTracks_d0, LeadTauAllTracks_z0, LeadTauAllTracksTheta, TauNTracks[0], TauNTracksIsolation[0])
                    SubleadTauAllTracksEta = evt.SubleadTauAllTracksEta
                    SubleadTauAllTracksPhi = evt.SubleadTauAllTracksPhi
                    SubleadTauAllTracksPt = evt.SubleadTauAllTracksPt
                    SubleadTauAllTracksE = evt.SubleadTauAllTracksE
                    SubleadTauAllTracks_d0 = evt.SubleadTauAllTracks_d0
                    SubleadTauAllTracks_z0 = evt.SubleadTauAllTracks_z0
                    SubleadTauAllTracksTheta = evt.SubleadTauAllTracksTheta
                    SubleadTauAllTracksDeltaEta, SubleadTauAllTracksDeltaPhi = angle_dist( TauEta[1], TauPhi[1], SubleadTauAllTracksEta, SubleadTauAllTracksPhi )
                    appender( array,  SubleadTauAllTracksDeltaEta, SubleadTauAllTracksDeltaPhi, SubleadTauAllTracksPt, SubleadTauAllTracksE,
                             SubleadTauAllTracks_d0, SubleadTauAllTracks_z0, SubleadTauAllTracksTheta, TauNTracks[1], TauNTracksIsolation[1])  
            else : #It enters here only if CHARGE_SELECTOR is off, and so works on true tau
                if len(TauEta) == 2 :
                    if (TauPt[0]/1000 > 20) and (TauPt[1]/1000 > 20) and (TauNTracks[0] == 1 or TauNTracks[0] == 3) and (TauNTracks[1] == 1 or TauNTracks[1] == 3) :
                        EVENT_SELECTOR = 1
                        if (np.abs(TauEta[0]) < 1.37 or (np.abs(TauEta[0]) > 1.52 and np.abs(TauEta[0]) < 2.5) ) and (np.abs(TauEta[1]) < 1.37 or (np.abs(TauEta[1]) > 1.52 and np.abs(TauEta[1]) < 2.5) ) :
                            EVENT_SELECTOR = 2
                    else :
                        EVENT_SELECTOR = 0 #It appens when one tau-filter is False
                else :
                    EVENT_SELECTOR = 0 #It appens if we do't have two true tau
                if EVENT_SELECTOR == 2:#Lead and Sublead Tau
                    tau_counter += 2
                    TauNTracks = evt.TauNTracks
                    TauNTracksIsolation = evt.TauNTracksIsolation
                    LeadTauAllTracksEta = evt.LeadTauAllTracksEta
                    LeadTauAllTracksPhi = evt.LeadTauAllTracksPhi
                    LeadTauAllTracksPt = evt.LeadTauAllTracksPt
                    LeadTauAllTracksE = evt.LeadTauAllTracksE
                    LeadTauAllTracks_d0 = evt.LeadTauAllTracks_d0
                    LeadTauAllTracks_z0 = evt.LeadTauAllTracks_z0
                    LeadTauAllTracksTheta = evt.LeadTauAllTracksTheta
                    LeadTauAllTracksDeltaEta, LeadTauAllTracksDeltaPhi = angle_dist( TauEta[0], TauPhi[0], LeadTauAllTracksEta, LeadTauAllTracksPhi )
                    appender( array,  LeadTauAllTracksDeltaEta, LeadTauAllTracksDeltaPhi, LeadTauAllTracksPt, LeadTauAllTracksE,
                             LeadTauAllTracks_d0, LeadTauAllTracks_z0, LeadTauAllTracksTheta, TauNTracks[0], TauNTracksIsolation[0])
                    SubleadTauAllTracksEta = evt.SubleadTauAllTracksEta
                    SubleadTauAllTracksPhi = evt.SubleadTauAllTracksPhi
                    SubleadTauAllTracksPt = evt.SubleadTauAllTracksPt
                    SubleadTauAllTracksE = evt.SubleadTauAllTracksE
                    SubleadTauAllTracks_d0 = evt.SubleadTauAllTracks_d0
                    SubleadTauAllTracks_z0 = evt.SubleadTauAllTracks_z0
                    SubleadTauAllTracksTheta = evt.SubleadTauAllTracksTheta
                    SubleadTauAllTracksDeltaEta, SubleadTauAllTracksDeltaPhi = angle_dist( TauEta[1], TauPhi[1], SubleadTauAllTracksEta, SubleadTauAllTracksPhi )
                    appender( array,  SubleadTauAllTracksDeltaEta, SubleadTauAllTracksDeltaPhi, SubleadTauAllTracksPt, SubleadTauAllTracksE,
                             SubleadTauAllTracks_d0, SubleadTauAllTracks_z0, SubleadTauAllTracksTheta, TauNTracks[1], TauNTracksIsolation[1])  
    input_file.Close()
if CHARGE_SELECTOR == 1:
    array = np.reshape(array, (Fake_tau_counter, 10, 8) )
else:
    array = np.reshape(array, (tau_counter, 10, 8) )
print("CHARGE SELECTOR is = ", CHARGE_SELECTOR)
print("The lenght of the array is ", len(array)," depending on CHARGE SELECTOR these are tau or fake tau")
print("The lenght of one tau of array is ", len(array[0])," and they should be 10 racks")
print("The lenght of one tau of array is ", len(array[0][0])," and they are: DEta, DPhi, Pt, E, d0, z0, theta, track-flag")

if CHARGE_SELECTOR == 1:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/dataset_fake_tau.npy', array)
else:
    np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/dataset_tau.npy', array)
