
import ROOT
import glob
import numpy as np


#path for ntuples hh
#path = '/afs/cern.ch/user/d/dafiacco/eos/ntuples/'
#path for ntples of dgcnn variables
path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/'
#path for code test
#path = '/afs/cern.ch/user/g/gpadovan/eos/HMDY/ntuples/Ztautau_TAUP3/tmp/user.gpadovan.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.MC16d.v1.2022-01-11_histograms.root/'
#path per le ntuple di Federico
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/Ztautau'
#path = '/afs/cern.ch/user/f/fmorodei/public/ntuple_per_Davide/data/'

file_counter = 0
tau_counter = 0
JT_counter = 0
array = []


print("In the directory there are ",len(glob.glob(path+"*20_histograms.root"))," root files")
for f in glob.glob(path+"*20_histograms.root"): #files:
    input_file=ROOT.TFile.Open(f) #path+f.name)
    file_counter += 1
    print("processing file: ", file_counter, "...")
    for evt in input_file.T_Zp2thh_NOMINAL:  ## loop over the events
        TauEta = evt.TauEta
        JetEta = evt.JetEta
        if len(TauEta) > 0 :
            TauPhi = evt.TauPhi
            TauE = evt.TauE
            TauPt = evt.TauPt
            for i in range( len(TauEta) ):
                JT_counter += 1
                tau_counter += 1
                array.append( [TauEta[i], TauPhi[i], TauE[i], TauPt[i], 1.0] )
        if len(JetEta) > 0 :
            JetPhi = evt.JetPhi
            JetE = evt.JetE
            JetPt = evt.JetPt
            for i in range( len(JetEta) ):
                JT_counter += 1
                array.append( [JetEta[i], JetPhi[i], JetE[i], JetPt[i], 0.0] )
    input_file.Close()
array = np.reshape(array, (JT_counter, 5) )
print("The lenght of the array is ", len(array)," These are Jets and Tau")
print("The lenght of one tau of array is ", len(array[0])," and should be 5: Eta, Phi, E, Pt, label")
print("The number of Tau is = ",tau_counter ,"and the number of Jet is = ", JT_counter-tau_counter )

np.save( '/afs/cern.ch/user/d/dafiacco/eos/DGCNN/dataset_JT.npy', array)
