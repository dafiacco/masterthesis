# -*- coding: utf-8 -*-
# +
#import tensorflow as tf

# +
#import numpy as np
#import datetime
#import matplotlib as mpl
#import matplotlib.pyplot as plt
#import pandas as pd
#import random
#import seaborn as sns
# -

# from tensorflow import keras

# +
#from sklearn.metrics import (
#    confusion_matrix,
#    roc_auc_score,
#    roc_curve,
#    precision_recall_curve,
#    auc,
#)
#from sklearn.utils import shuffle

# +
# see if we can control randomness in results
#from numpy.random import seed

# +
#seed(1)
#tf.random.set_seed(2)

# +
# function that creates a pandas dataframe with prediction and ground_truth, and also recon_val
def create_df_reconstruction( dict_data, reconstruction_error_val, threshold_val): #it works also with kls method

    df = pd.DataFrame(data=reconstruction_error_val, columns=["recon_val"])
    
    # label anomolous (outlier) data as -1, inliers as 1
    # -1 (outlier) is POSITIVE class
    #  1 (inlier) is NEGATIVE class
    new_y_data = []
    for i in range(len(dict_data['labels'])):
        if dict_data['labels'][i] == 0:
            new_y_data.append(-1) #qcd
        else:
            new_y_data.append(1) #tau

    df["ground_truth"] = new_y_data

    # add prediction based on threshold
    df["prediction"] = np.where( df['recon_val'] >= threshold_val, -1, 1) #se è vero -1, altrimenti 1

    return df

#actually it only returns precisions and recall... but in future it will use ROC curve to get best threshold
def threshold_grid_search(
    dict_data,
    lower_bound,
    upper_bound,
    reconstruction_error_val,
    grid_iterations=10,
):
    """Simple grid search for finding the best threshold"""

    roc_scores = {}
    tprs = []  # true positive rates
    fprs = []  # false positive rates
    precisions = []
    recalls = []
    tp_scores = []
    tn_scores = []
    fp_scores = []
    fn_scores = []
    grid_search_count = 0
    
    grid = np.arange(lower_bound, upper_bound/40, np.abs(upper_bound/40-lower_bound)*4/(3*grid_iterations) )
    grid = np.append(grid, np.arange(upper_bound/40, upper_bound, np.abs(upper_bound - upper_bound/40)*4/(grid_iterations)) )


    for i in grid:
        #             if grid_search_count%50 == 0:
        #                 print('grid search iteration: ', grid_search_count)

        threshold_val = i
        df = create_df_reconstruction(
            dict_data, reconstruction_error_val, threshold_val #here latent version take y and kls and threshold_val
        )
        
        roc_val = roc_auc_score(df["ground_truth"], df["prediction"])
        roc_scores[i] = roc_val
        

        grid_search_count += 1

        # calculate precision and recall
        # True Positive
        tp = len(df[(df["ground_truth"] == -1) & (df["prediction"] == -1)])
        # False Positive -- predict anomaly (-1), when it is actually normal (1)
        fp = len(df[(df["ground_truth"] == 1) & (df["prediction"] == -1)])
        # True Negative
        tn = len(df[(df["ground_truth"] == 1) & (df["prediction"] == 1)])
        # False Negative
        fn = len(df[(df["ground_truth"] == -1) & (df["prediction"] == 1)])
        
        tp_scores.append( tp )
        tn_scores.append( tn )
        fp_scores.append( fp )
        fn_scores.append( fn )
        # print('threshold val', i)
        # print('tp:',tp,'fp:', fp, 'tn:', tn, 'fn:',fn)

        try:

            # precision/recall
            pre_score = tp / (tp + fp)
            re_score = tp / (tp + fn)
            # tpr/fpr
            tpr = tp / (tp + fn)
            fpr = fp / (fp + tn)

            precisions.append(pre_score)
            recalls.append(re_score)
            tprs.append(tpr)
            fprs.append(fpr)

        except ZeroDivisionError as err: #I punti dove recall è nullo vengono ignorati
            pass
            # print('Handling run-time error:', err)

    # return best roc_score and the threshold used to set it #roc_scores.keys() are threshold values used in the for
    threshold_val = max(zip(roc_scores.values(), roc_scores.keys())) #We can use the zip() function to merge our two lists. Here is an example program that will merge this data
    best_threshold = threshold_val[1]
    best_roc_score = threshold_val[0]
    print('Best threshold:', '{:.5}'.format(best_threshold),'\tROC score: {:.2%}'.format(best_roc_score))

    return best_threshold, fprs, tprs, precisions, recalls

def mse(X_val, recon_val):
    """Calculate MSE for images in X_val and recon_val"""
    return np.mean( np.square(X_val - recon_val), axis=1 )#.astype("float32")

def input_anomaly_detection(
    model,
    dict_data,
    grid_iterations=10,
    search_iterations=1,
    curve_values=False,
):
    """Function to test Precision-Recall of Input A.D. 

    Parameters
    ===========
    model : tensorflow model
        autoencoder model that was trained on the "slim" data set.
        Will be used to build reconstructions

    dict_data :
        tensor of the X validation set

    """
    reconstructed_data = model.predict(dict_data['features'], batch_size=256, verbose=1,)

    # run through each of the reconstruction error methods, perform a little grid search
    # to find the optimum value

    # _______MSE_______#
    # calculate MSE reconstruction error
    mse_reconstructed = mse(
        dict_data['features'], reconstructed_data
    )
    print("mse_reconstructed shape = ",mse_reconstructed.shape)
    # calculate pr-auc and roc-auc for data set
    lower_bound = np.min(mse_reconstructed)
    upper_bound = np.max(mse_reconstructed)
    print("lower bound = ", lower_bound)
    print("upper bound = ", upper_bound)
    #upper_bound = 0.1
    print("Forced upper bound = ", upper_bound)
    print("threshold grid starts...")
    (
        best_threshold,
        fprs,
        tprs,
        precisions,
        recalls,
    ) = threshold_grid_search( dict_data, lower_bound, upper_bound, mse_reconstructed, grid_iterations )


    pr_auc_score_recon = auc(recalls, precisions)
    roc_auc_score_recon = auc(fprs, tprs)

    if curve_values :
        return best_threshold, roc_auc_score_recon, pr_auc_score_recon, recalls, precisions, fprs, tprs
    else :
        return best_threshold, roc_auc_score_recon, pr_auc_score_recon

def kl_divergence(mu, log_var):
    return -0.5 * K.sum(1 + log_var - K.exp(log_var) - K.square(mu), axis=-1,)

def pr_auc_kl(
    encoder,
    dict_data,#X,y,
    grid_iterations=10
):

    """
    Function that gets the precision and recall scores for the encoder (and so, in the latent space)
    """

    codings_mean, codings_log_var, codings = encoder.predict(dict_data['features'], batch_size=256)

    kls = np.array(kl_divergence(codings_mean, codings_log_var))
    kls = np.reshape(kls, (-1, 1))

    lower_bound = np.min(kls)
    upper_bound = np.max(kls)
    print("lower bound = ", lower_bound)
    print("upper bound = ", upper_bound)
    print("threshold grid starts...")
    (
        best_threshold,
        fprs,
        tprs,
        precisions,
        recalls
    ) = threshold_grid_search( #y instead od dict data of reconstruction_method
        dict_data, lower_bound, upper_bound, kls, grid_iterations,
    )

    pr_auc_score = auc(recalls, precisions)
    roc_auc_score = auc(fprs, tprs)
    
    return (
        pr_auc_score,
        roc_auc_score,
        recalls,
        precisions,
        tprs,
        fprs,
        best_threshold,
    )

def latent_anomaly_detection( ###NoN prende nome cartella e nome modello, ma direttamente il modello
    encoder_model,
    dict_data,
    grid_iterations,
    search_iterations,
    curve_values = False,
):

    pr_auc_kls = []
    roc_auc_kls = []
    recalls_array = []
    precisions_array = []
    tprs_array = []
    fprs_array = []

    for i in range(search_iterations):
        print("search_iter:", i)
        (
            pr_auc_score,
            roc_auc_score,
            recalls,
            precisions,
            tprs,
            fprs,
            best_threshold_kl,
        ) = pr_auc_kl(
            encoder_model,
            dict_data,
            grid_iterations=grid_iterations
        )

        pr_auc_kls.append(pr_auc_score)
        roc_auc_kls.append(roc_auc_score)
        recalls_array.append(recalls)
        precisions_array.append(precisions)
        tprs_array.append(tprs)
        fprs_array.append(fprs)
        
    pr_auc_score_kl = np.mean(np.array(pr_auc_kls)) #La media serve se vuole fare più test
    roc_auc_score_kl = np.mean(np.array(roc_auc_kls))
    recalls_array = np.array(recalls_array)
    precisions_array = np.array(precisions_array)
    tprs_array = np.array(tprs_array)
    fprs_array = np.array(fprs_array)

    if curve_values :
        return best_threshold_kl, roc_auc_score_kl, pr_auc_score_kl, recalls_array, precisions_array, fprs_array, tprs_array
    else :
        return best_threshold_kl, roc_auc_score_kl, pr_auc_score_kl

def build_kls_scores(encoder, X,):
    """Get the KL-divergence scores across from a trained VAE encoder.
 
    Parameters
    ===========
    encoder : TenorFlow model
        Encoder of the VAE
    
    X : tensor
        data that KL-div. scores will be calculated from

    Returns
    ===========
    kls : numpy array
        Returns the KL-divergence scores as a numpy array
    """
    codings_mean, codings_log_var, codings = encoder.predict(X, batch_size=64)
    kls = np.array(kl_divergence(codings_mean, codings_log_var))
    return kls
# -


